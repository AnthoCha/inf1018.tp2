﻿using Inf1018.Tp2.Core.Exceptions;
using Inf1018.Tp2.Resources;

namespace Inf1018.Tp2.Lex.Exceptions
{
    public class IdentifierTooLongException : LexException
    {
        public string Identifier { get; }

        public IdentifierTooLongException(string identifier)
        {
            Identifier = identifier;
        }

        public override string Message
        {
            get
            {
                return string.Join(' ', base.Message, string.Format(ErrorMessages.IdentifierTooLong, Identifier, Constants.IdentifierMaxLength));
            }
        }
    }
}
