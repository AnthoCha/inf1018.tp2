﻿using Inf1018.Tp2.Core.Exceptions;
using Inf1018.Tp2.Resources;

namespace Inf1018.Tp2.Lex.Exceptions
{
    public class UnexpectedCharacterException : LexException
    {
        public int Character { get; }


        public UnexpectedCharacterException(int character)
        {
            Character = character;
        }

        public override string Message
        {
            get
            {
                return string.Join(' ', base.Message, string.Format(ErrorMessages.UnexpectedCharacter, char.ConvertFromUtf32(Character)));
            }
        }
    }
}
