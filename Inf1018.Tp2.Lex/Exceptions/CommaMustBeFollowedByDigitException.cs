﻿using Inf1018.Tp2.Resources;

namespace Inf1018.Tp2.Lex.Exceptions
{
    public class CommaMustBeFollowedByDigitException : UnexpectedCharacterException
    {
        public CommaMustBeFollowedByDigitException(int character) : base(character)
        {
        }

        public override string Message
        {
            get
            {
                return string.Join(' ', base.Message, ErrorMessages.CommaMustBeFollowedByDigit);
            }
        }
    }
}
