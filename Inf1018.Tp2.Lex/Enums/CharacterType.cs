﻿namespace Inf1018.Tp2.Lex.Enums
{
    public enum CharacterType
    {
        Unknown,
        Reserved,
        Letter,
        Digit,
        WhiteSpace,
        Comma,
        Underscore
    }
}
