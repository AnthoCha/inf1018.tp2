﻿using Inf1018.Tp2.Core;
using Inf1018.Tp2.Core.Enums;
using Inf1018.Tp2.Core.Interfaces;
using Inf1018.Tp2.Lex.Enums;
using Inf1018.Tp2.Lex.Exceptions;
using System.IO;
using System.Text;

namespace Inf1018.Tp2.Lex
{
    /// <summary>
    /// Représente un lecteur d'unités lexicales.
    /// </summary>
    public class LexReader : ILexReader
    {
        private readonly TextReader _reader;

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="LexReader"/> avec le lecteur de texte spécifié.
        /// </summary>
        /// <param name="reader">Lecteur de texte.</param>
        public LexReader(TextReader reader)
        {
            _reader = reader;
        }

        /// <inheritdoc/>
        public Token Current { get; private set; }

        /// <inheritdoc/>
        public Token Read()
        {
            int currentChar;
            CharacterType currentCharType;

            while ((currentChar = _reader.Peek()) != -1)
            {
                currentCharType = GetCharacterType(currentChar);

                switch (currentCharType)
                {
                    case CharacterType.Reserved:
                        return Current = GetReservedCharacterToken(_reader.Read());
                    case CharacterType.Letter:
                    case CharacterType.Underscore:
                        var identifier = new StringBuilder();

                        while (currentCharType == CharacterType.Letter || currentCharType == CharacterType.Digit || currentCharType == CharacterType.Underscore)
                        {
                            identifier.Append(char.ConvertFromUtf32(_reader.Read()));
                            currentChar = _reader.Peek();
                            currentCharType = GetCharacterType(currentChar);
                        }

                        return Current = GetIdentifierOrKeywordToken(identifier.ToString());
                    case CharacterType.Digit:
                        while (currentCharType == CharacterType.Digit)
                        {
                            _reader.Read();
                            currentChar = _reader.Peek();
                            currentCharType = GetCharacterType(currentChar);
                        }

                        if (currentCharType == CharacterType.Comma)
                        {
                            _reader.Read();
                            currentChar = _reader.Peek();
                            currentCharType = GetCharacterType(currentChar);

                            if (currentCharType == CharacterType.Digit)
                            {
                                while (currentCharType == CharacterType.Digit)
                                {
                                    _reader.Read();
                                    currentChar = _reader.Peek();
                                    currentCharType = GetCharacterType(currentChar);
                                }
                            }
                            else
                            {
                                throw new CommaMustBeFollowedByDigitException(currentChar);
                            }
                        }

                        return Current = new Token(TokenType.Nombre);
                    case CharacterType.WhiteSpace:
                        _reader.Read();
                        break;
                    default:
                        throw new UnexpectedCharacterException(currentChar);
                }
            }

            return Current = null;
        }

        private CharacterType GetCharacterType(int character)
        {
            switch (character)
            {
                case ':':
                case ';':
                case '=':
                case '+':
                case '-':
                case '*':
                case '/':
                case '(':
                case ')':
                    return CharacterType.Reserved;
                case ' ':
                case '\n':
                case '\r':
                case '\t':
                    return CharacterType.WhiteSpace;
                case ',':
                    return CharacterType.Comma;
                case '_':
                    return CharacterType.Underscore;
                default:
                    if (character >= '0' && character <= '9')
                    {
                        return CharacterType.Digit;
                    }
                    else if ((character >= 'A' && character <= 'Z') || (character >= 'a' && character <= 'z'))
                    {
                        return CharacterType.Letter;
                    }
                    else
                    {
                        return CharacterType.Unknown;
                    }
            }
        }

        private Token GetReservedCharacterToken(int character)
        {
            switch (character)
            {
                case ':':
                    return new Token(TokenType.DeuxPoints);
                case ';':
                    return new Token(TokenType.PointVirgule);
                case '=':
                    return new Token(TokenType.Egale);
                case '+':
                    return new Token(TokenType.Addition);
                case '-':
                    return new Token(TokenType.Soustraction);
                case '*':
                    return new Token(TokenType.Multiplication);
                case '/':
                    return new Token(TokenType.Division);
                case '(':
                    return new Token(TokenType.ParantheseOuvrante);
                case ')':
                    return new Token(TokenType.ParantheseFermante);
                default:
                    throw new UnexpectedCharacterException(character);
            }
        }

        private Token GetIdentifierOrKeywordToken(string identifier)
        {
            switch (identifier)
            {
                case "Procedure":
                    return new Token(TokenType.Procedure);
                case "Fin_Procedure":
                    return new Token(TokenType.FinProcedure);
                case "declare":
                    return new Token(TokenType.Declare);
                case "entier":
                    return new Token(TokenType.Entier);
                case "reel":
                    return new Token(TokenType.Reel);
                default:
                    if (identifier.Length > Constants.IdentifierMaxLength)
                    {
                        throw new IdentifierTooLongException(identifier);
                    }

                    return new IdentifierToken(identifier);
            }
        }
    }
}
