﻿using Inf1018.Tp2.Core;
using Inf1018.Tp2.Core.Enums;
using Inf1018.Tp2.Lex;
using Inf1018.Tp2.Lex.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Text;

namespace Inf1018.Tp2.Tests
{
    [TestClass]
    public class LexReaderTest
    {
        [TestMethod]
        public void WithDeuxPoints_ReturnsDeuxPoints()
        {
            // Arrange
            var expected = new Token(TokenType.DeuxPoints);

            var sb = new StringBuilder();
            sb.Append(':');

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithPointVirgule_ReturnsPointVirgule()
        {
            // Arrange
            var expected = new Token(TokenType.PointVirgule);

            var sb = new StringBuilder();
            sb.Append(';');

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithEgale_ReturnsEgale()
        {
            // Arrange
            var expected = new Token(TokenType.Egale);

            var sb = new StringBuilder();
            sb.Append('=');

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithAddition_ReturnsAddition()
        {
            // Arrange
            var expected = new Token(TokenType.Addition);

            var sb = new StringBuilder();
            sb.Append('+');

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithSoustraction_ReturnsSoustraction()
        {
            // Arrange
            var expected = new Token(TokenType.Soustraction);

            var sb = new StringBuilder();
            sb.Append('-');

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithMultiplication_ReturnsMultiplication()
        {
            // Arrange
            var expected = new Token(TokenType.Multiplication);

            var sb = new StringBuilder();
            sb.Append('*');

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithDivision_ReturnsDivision()
        {
            // Arrange
            var expected = new Token(TokenType.Division);

            var sb = new StringBuilder();
            sb.Append('/');

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithParantheseOuvrante_ReturnsParantheseOuvrante()
        {
            // Arrange
            var expected = new Token(TokenType.ParantheseOuvrante);

            var sb = new StringBuilder();
            sb.Append('(');

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithParantheseFermante_ReturnsParantheseFermante()
        {
            // Arrange
            var expected = new Token(TokenType.ParantheseFermante);

            var sb = new StringBuilder();
            sb.Append(')');

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithProcedure_ReturnsProcedure()
        {
            // Arrange
            var expected = new Token(TokenType.Procedure);

            var sb = new StringBuilder();
            sb.Append("Procedure");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithFin_Procedure_ReturnsFinProcedure()
        {
            // Arrange
            var expected = new Token(TokenType.FinProcedure);

            var sb = new StringBuilder();
            sb.Append("Fin_Procedure");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithDeclare_ReturnsDeclare()
        {
            // Arrange
            var expected = new Token(TokenType.Declare);

            var sb = new StringBuilder();
            sb.Append("declare");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithEntier_ReturnsEntier()
        {
            // Arrange
            var expected = new Token(TokenType.Entier);

            var sb = new StringBuilder();
            sb.Append("entier");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithReel_ReturnsReel()
        {
            // Arrange
            var expected = new Token(TokenType.Reel);

            var sb = new StringBuilder();
            sb.Append("reel");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithIdentifier_ReturnsIdentifier()
        {
            // Arrange
            var expected = new IdentifierToken("var");

            var sb = new StringBuilder();
            sb.Append("var");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithIdentifier_WithDigit_ReturnsIdentifier()
        {
            // Arrange
            var expected = new IdentifierToken("var1");

            var sb = new StringBuilder();
            sb.Append("var1");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithIdentifier_WithUnderscore_ReturnsIdentifier()
        {
            // Arrange
            var expected = new IdentifierToken("_var_1");

            var sb = new StringBuilder();
            sb.Append("_var_1");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithIdentifier_ThenReservedCharacter_ReturnsIdentifier()
        {
            // Arrange
            var expected = new IdentifierToken("var");

            var sb = new StringBuilder();
            sb.Append("var+");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithIdentifier_ThenWhiteSpaceCharacter_ReturnsIdentifier()
        {
            // Arrange
            var expected = new IdentifierToken("var");

            var sb = new StringBuilder();
            sb.Append("var ");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithIdentifier_GreaterThanIdentifierMaxLength_ThrowsIdentifierTooLongException()
        {
            // Arrange
            var sb = new StringBuilder();
            sb.Append("identifier");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<IdentifierTooLongException>(action);
        }

        [TestMethod]
        public void WithDigit_ReturnsNombre()
        {
            // Arrange
            var expected = new Token(TokenType.Nombre);

            var sb = new StringBuilder();
            sb.Append('0');

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithDigits_ReturnsNombre()
        {
            // Arrange
            var expected = new Token(TokenType.Nombre);

            var sb = new StringBuilder();
            sb.Append("42");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithDigits_ThenComma_ThenDecimal_ReturnsNombre()
        {
            // Arrange
            var expected = new Token(TokenType.Nombre);

            var sb = new StringBuilder();
            sb.Append("42,0");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithDigits_ThenComma_ThenDigits_ReturnsNombre()
        {
            // Arrange
            var expected = new Token(TokenType.Nombre);

            var sb = new StringBuilder();
            sb.Append("42,42");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            var actual = reader.Read();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WithDigits_ThenComma_ThenNothing_ThrowsCommaMustBeFollowedByDigitException()
        {
            // Arrange
            var sb = new StringBuilder();
            sb.Append("42,");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<CommaMustBeFollowedByDigitException>(action);
        }

        [TestMethod]
        public void WithDigits_ThenComma_ThenCharacterOtherThanDigit_ThrowsCommaMustBeFollowedByDigitException()
        {
            // Arrange
            var sb = new StringBuilder();
            sb.Append("42,a");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<CommaMustBeFollowedByDigitException>(action);
        }

        [TestMethod]
        public void WithUnknownCharacter_ThrowsUnexpectedCharacterException()
        {
            // Arrange
            var sb = new StringBuilder();
            sb.Append("!");

            using var textReader = new StringReader(sb.ToString());
            var reader = new LexReader(textReader);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<UnexpectedCharacterException>(action);
        }
    }
}
