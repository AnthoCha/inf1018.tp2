﻿using Inf1018.Tp2.Lex;
using Inf1018.Tp2.Syntax;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Text;

namespace Inf1018.Tp2.Tests
{
    [TestClass]
    public class ExpressionArithmetiqueSyntaxReaderTest
    {
        [TestMethod]
        public void WithOneTerme()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("1*2+3) * 42");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new ExpressionArithmetiqueSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
        }

        [TestMethod]
        public void WithTwoTermes_WithAddition()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("(1*2+3) * 42 + 0");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new ExpressionArithmetiqueSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
        }

        [TestMethod]
        public void WithTwoTermes_WithSoustraction()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("(1*2+3) * 42 - 0");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new ExpressionArithmetiqueSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
        }
    }
}
