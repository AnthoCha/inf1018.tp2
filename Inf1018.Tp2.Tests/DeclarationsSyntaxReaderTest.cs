﻿using Inf1018.Tp2.Lex;
using Inf1018.Tp2.Syntax;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Text;

namespace Inf1018.Tp2.Tests
{
    [TestClass]
    public class DeclarationsSyntaxReaderTest
    {
        [TestMethod]
        public void WithOneDeclaration_DeclaresVariable()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("declare var : entier;");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new DeclarationsSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
            Assert.IsTrue(context.IsVariableDeclared("var"));
        }

        [TestMethod]
        public void WithTwoDeclarations_DeclaresBothVariables()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.AppendLine("declare var : entier;");
            sb.AppendLine("declare var1 : entier;");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new DeclarationsSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
            Assert.IsTrue(context.IsVariableDeclared("var") && context.IsVariableDeclared("var1"));
        }

        [TestMethod]
        public void WithThreeDeclarations_DeclaresThreeVariables()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.AppendLine("declare var : entier;");
            sb.AppendLine("declare var1 : entier;");
            sb.AppendLine("declare _var_1 : reel;");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new DeclarationsSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
            Assert.IsTrue(context.IsVariableDeclared("var") && context.IsVariableDeclared("var1") && context.IsVariableDeclared("_var_1"));
        }
    }
}
