﻿using Inf1018.Tp2.Lex;
using Inf1018.Tp2.Syntax;
using Inf1018.Tp2.Syntax.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Text;

namespace Inf1018.Tp2.Tests
{
    [TestClass]
    public class ProcedureSyntaxReaderTest
    {
        [TestMethod]
        public void WithSameProcedureIdentifier()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.AppendLine("Procedure proc");
            sb.AppendLine("declare var : entier;");
            sb.AppendLine("var = (1*2+3) * 42 + 0;");
            sb.AppendLine("var = var / 42 - 1,5 * 4");
            sb.AppendLine("Fin_Procedure proc");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new ProcedureSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
        }

        [TestMethod]
        public void WithMissingProcedureToken_ThrowsMissingTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new ProcedureSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<MissingTokenException>(action);
        }

        [TestMethod]
        public void WithOtherProcedureToken_ThrowsUnexpectedTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("42");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new ProcedureSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<UnexpectedTokenException>(action);
        }

        [TestMethod]
        public void WithMissingIdentifier1Token_ThrowsMissingTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("Procedure");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new ProcedureSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<MissingTokenException>(action);
        }

        [TestMethod]
        public void WithOtherIdentifier1Token_ThrowsUnexpectedTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("Procedure 42");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new ProcedureSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<UnexpectedTokenException>(action);
        }

        [TestMethod]
        public void WithMissingFinProcedureToken_ThrowsMissingTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.AppendLine("Procedure proc");
            sb.AppendLine("declare var : entier;");
            sb.AppendLine("var = (1*2+3) * 42 + 0;");
            sb.AppendLine("var = var / 42 - 1,5 * 4");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new ProcedureSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<MissingTokenException>(action);
        }

        [TestMethod]
        public void WithOtherFinProcedureToken_ThrowsUnexpectedTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.AppendLine("Procedure proc");
            sb.AppendLine("declare var : entier;");
            sb.AppendLine("var = (1*2+3) * 42 + 0;");
            sb.AppendLine("var = var / 42 - 1,5 * 4");
            sb.AppendLine("42");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new ProcedureSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<UnexpectedTokenException>(action);
        }

        [TestMethod]
        public void WithMissingIdentifier2Token_ThrowsMissingTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.AppendLine("Procedure proc");
            sb.AppendLine("declare var : entier;");
            sb.AppendLine("var = (1*2+3) * 42 + 0;");
            sb.AppendLine("var = var / 42 - 1,5 * 4");
            sb.AppendLine("Fin_Procedure");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new ProcedureSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<MissingTokenException>(action);
        }

        [TestMethod]
        public void WithOtherIdentifier2Token_ThrowsUnexpectedTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.AppendLine("Procedure proc");
            sb.AppendLine("declare var : entier;");
            sb.AppendLine("var = (1*2+3) * 42 + 0;");
            sb.AppendLine("var = var / 42 - 1,5 * 4");
            sb.AppendLine("Fin_Procedure 42");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new ProcedureSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<UnexpectedTokenException>(action);
        }

        [TestMethod]
        public void WithDifferentProcedureIdentifiers_ThrowsProcedureIdentifiersDoNotMatchException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.AppendLine("Procedure proc");
            sb.AppendLine("declare var : entier;");
            sb.AppendLine("var = (1*2+3) * 42 + 0;");
            sb.AppendLine("var = var / 42 - 1,5 * 4");
            sb.AppendLine("Fin_Procedure proc1");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new ProcedureSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<ProcedureIdentifiersDoNotMatchException>(action);
        }
    }
}
