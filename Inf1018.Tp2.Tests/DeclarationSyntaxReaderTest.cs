﻿using Inf1018.Tp2.Lex;
using Inf1018.Tp2.Syntax;
using Inf1018.Tp2.Syntax.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Text;

namespace Inf1018.Tp2.Tests
{
    [TestClass]
    public class DeclarationSyntaxReaderTest
    {
        [TestMethod]
        public void WithNewVariable_DelcaresVariable()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("declare var : entier;");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new DeclarationSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
            Assert.IsTrue(context.IsVariableDeclared("var"));
        }

        [TestMethod]
        public void WithMissingDeclareToken_ThrowsMissingTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new DeclarationSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<MissingTokenException>(action);
        }

        [TestMethod]
        public void WithOtherDeclareToken_ThrowsUnexpectedTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("42 var : entier;");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new DeclarationSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<UnexpectedTokenException>(action);
        }

        [TestMethod]
        public void WithMissingDeuxPointsToken_ThrowsMissingTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("declare var ");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new DeclarationSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<MissingTokenException>(action);
        }

        [TestMethod]
        public void WithOtherDeuxPointsToken_ThrowsUnexpectedTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("declare var 42 entier;");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new DeclarationSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<UnexpectedTokenException>(action);
        }

        [TestMethod]
        public void WithMissingPointVirguleToken_ThrowsMissingTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("declare var : entier");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new DeclarationSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<MissingTokenException>(action);
        }

        [TestMethod]
        public void WithOtherPointVirguleToken_ThrowsUnexpectedTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("declare var : entier 42");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new DeclarationSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<UnexpectedTokenException>(action);
        }
    }
}
