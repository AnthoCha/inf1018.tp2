﻿using Inf1018.Tp2.Lex;
using Inf1018.Tp2.Syntax;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Text;

namespace Inf1018.Tp2.Tests
{
    [TestClass]
    public class TermeSyntaxReaderTest
    {
        [TestMethod]
        public void WithOneFacteur()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("(1*2+3)");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new TermeSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
        }

        [TestMethod]
        public void WithTwoFacteurs_WithMultiplication()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("(1*2+3) * 42");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new TermeSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
        }

        [TestMethod]
        public void WithTwoFacteurs_WithDivision()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("(1*2+3) / 42");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new TermeSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
        }
    }
}
