﻿using Inf1018.Tp2.Lex;
using Inf1018.Tp2.Syntax;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Text;

namespace Inf1018.Tp2.Tests
{
    [TestClass]
    public class InstructionsAffectationSyntaxReaderTest
    {
        [TestMethod]
        public void WithOneInstructionAffectation()
        {
            // Arrange
            var context = new SyntaxReaderContext();
            context.DeclareVariable("var");

            var sb = new StringBuilder();
            sb.Append("var = (1*2+3) * 42 + 0");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new InstructionsAffectationSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
        }

        [TestMethod]
        public void WithTwoInstructionsAffectation()
        {
            // Arrange
            var context = new SyntaxReaderContext();
            context.DeclareVariable("var");

            var sb = new StringBuilder();
            sb.AppendLine("var = (1*2+3) * 42 + 0;");
            sb.AppendLine("var = var / 42 - 1,5 * 4");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new InstructionsAffectationSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
        }
    }
}
