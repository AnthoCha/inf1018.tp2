﻿using Inf1018.Tp2.Lex;
using Inf1018.Tp2.Syntax;
using Inf1018.Tp2.Syntax.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Text;

namespace Inf1018.Tp2.Tests
{
    [TestClass]
    public class TypeSyntaxReaderTest
    {
        [TestMethod]
        public void WithEntierToken()
        {
            // Arrange
            var sb = new StringBuilder();
            sb.Append("entier");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new TypeSyntaxReader(lexReader);

            // Act
            reader.Read();

            // Assert
        }

        [TestMethod]
        public void WithReelToken()
        {
            // Arrange
            var sb = new StringBuilder();
            sb.Append("reel");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new TypeSyntaxReader(lexReader);

            // Act
            reader.Read();

            // Assert
        }

        [TestMethod]
        public void WithMissingToken_ThrowsMissingTokenException()
        {
            // Arrange
            var sb = new StringBuilder();

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new TypeSyntaxReader(lexReader);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<MissingTokenException>(action);
        }

        [TestMethod]
        public void WithOtherToken_ThrowsUnexpectedTokenException()
        {
            // Arrange
            var sb = new StringBuilder();
            sb.Append("42");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new TypeSyntaxReader(lexReader);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<UnexpectedTokenException>(action);
        }
    }
}
