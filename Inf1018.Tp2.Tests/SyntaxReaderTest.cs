﻿using Inf1018.Tp2.Lex;
using Inf1018.Tp2.Syntax;
using Inf1018.Tp2.Syntax.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Text;

namespace Inf1018.Tp2.Tests
{
    [TestClass]
    public class SyntaxReaderTest
    {
        [TestMethod]
        public void WithOneProcedure()
        {
            // Arrange
            var sb = new StringBuilder();
            sb.AppendLine("Procedure proc");
            sb.AppendLine("declare var : entier;");
            sb.AppendLine("var = (1*2+3) * 42 + 0;");
            sb.AppendLine("var = var / 42 - 1,5 * 4");
            sb.AppendLine("Fin_Procedure proc");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new SyntaxReader(lexReader);

            // Act
            reader.Read();

            // Assert
        }

        [TestMethod]
        public void WithAdditionalToken_ThrowsUnexpectedTokenException()
        {
            // Arrange
            var sb = new StringBuilder();
            sb.AppendLine("Procedure proc");
            sb.AppendLine("declare var : entier;");
            sb.AppendLine("var = (1*2+3) * 42 + 0;");
            sb.AppendLine("var = var / 42 - 1,5 * 4");
            sb.AppendLine("Fin_Procedure proc");
            sb.AppendLine("42");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new SyntaxReader(lexReader);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<UnexpectedTokenException>(action);
        }
    }
}
