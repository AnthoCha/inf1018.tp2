﻿using Inf1018.Tp2.Lex;
using Inf1018.Tp2.Syntax;
using Inf1018.Tp2.Syntax.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Text;

namespace Inf1018.Tp2.Tests
{
    [TestClass]
    public class InstructionAffectationSyntaxReaderTest
    {
        [TestMethod]
        public void WithDeclaredVariable()
        {
            // Arrange
            var context = new SyntaxReaderContext();
            context.DeclareVariable("var");

            var sb = new StringBuilder();
            sb.Append("var = (1*2+3) * 42 + 0");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new InstructionAffectationSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
        }

        [TestMethod]
        public void WithMissingEgaleToken_ThrowsMissingTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();
            context.DeclareVariable("var");

            var sb = new StringBuilder();
            sb.Append("var");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new InstructionAffectationSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<MissingTokenException>(action);
        }

        [TestMethod]
        public void WithOtherEgaleToken_ThrowsUnexpectedTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();
            context.DeclareVariable("var");

            var sb = new StringBuilder();
            sb.Append("var (1*2+3) * 42 + 0");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new InstructionAffectationSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<UnexpectedTokenException>(action);
        }
    }
}
