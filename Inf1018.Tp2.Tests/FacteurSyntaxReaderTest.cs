﻿using Inf1018.Tp2.Lex;
using Inf1018.Tp2.Syntax;
using Inf1018.Tp2.Syntax.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Text;

namespace Inf1018.Tp2.Tests
{
    [TestClass]
    public class FacteurSyntaxReaderTest
    {
        [TestMethod]
        public void WithDeclaredVariable()
        {
            // Arrange
            var context = new SyntaxReaderContext();
            context.DeclareVariable("var");

            var sb = new StringBuilder();
            sb.Append("var");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new FacteurSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
        }

        [TestMethod]
        public void WithNombreToken()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("42");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new FacteurSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
        }

        [TestMethod]
        public void WithPriorityExpression()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("(1*2+3)");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new FacteurSyntaxReader(lexReader, context);

            // Act
            reader.Read();

            // Assert
        }

        [TestMethod]
        public void WithMissingParantheseFermanteToken_ThrowsMissingTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("(1*2+3");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new FacteurSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<MissingTokenException>(action);
        }

        [TestMethod]
        public void WithOtherParatheseFermanteToken_ThrowsUnexpectedTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("(1*2+3 Fin_Procedure");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new FacteurSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<UnexpectedTokenException>(action);
        }

        [TestMethod]
        public void WithMissingToken_ThrowsMissingTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new FacteurSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<MissingTokenException>(action);
        }

        [TestMethod]
        public void WithOtherToken_ThrowsUnexpectedTokenException()
        {
            // Arrange
            var context = new SyntaxReaderContext();

            var sb = new StringBuilder();
            sb.Append("Fin_Procedure");

            using var textReader = new StringReader(sb.ToString());
            var lexReader = new LexReader(textReader);
            lexReader.Read();
            var reader = new FacteurSyntaxReader(lexReader, context);

            // Act
            Action action = () => reader.Read();

            // Assert
            Assert.ThrowsException<UnexpectedTokenException>(action);
        }
    }
}
