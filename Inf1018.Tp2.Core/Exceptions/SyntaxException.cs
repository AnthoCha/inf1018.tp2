﻿using Inf1018.Tp2.Resources;
using System;

namespace Inf1018.Tp2.Core.Exceptions
{
    public class SyntaxException : ApplicationException
    {
        public override string Message
        {
            get
            {
                return ErrorMessages.SyntaxError;
            }
        }
    }
}
