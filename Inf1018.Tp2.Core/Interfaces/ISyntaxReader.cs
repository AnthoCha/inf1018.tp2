﻿namespace Inf1018.Tp2.Core.Interfaces
{
    public interface ISyntaxReader
    {
        void Read();
    }
}
