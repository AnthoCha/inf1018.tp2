﻿using Inf1018.Tp2.Core.Exceptions;

namespace Inf1018.Tp2.Core.Interfaces
{
    /// <summary>
    /// Représente un lecteur d'unités lexicales.
    /// </summary>
    public interface ILexReader
    {
        /// <summary>
        /// Obtient l'unité lexicale en cours sans modifier l'état du lecteur.
        /// </summary>
        /// <returns>Retourne l'unité lexicale en cours, ou null si aucune unité lexicale n'a été lue ou plus aucune unité lexicale n'est disponible.</returns>
        Token Current { get; }

        /// <summary>
        /// Lit l'unité lexicale suivante et avance la position du lecteur.
        /// </summary>
        /// <returns>Retourne la prochaine unité lexicale disponible, ou null si plus aucune unité lexicale n'est disponible.</returns>
        /// <exception cref="LexException">Erreur lors de l'analyse lexicale.</exception>
        Token Read();
    }
}
