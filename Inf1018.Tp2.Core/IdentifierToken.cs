﻿using Inf1018.Tp2.Core.Enums;
using System;

namespace Inf1018.Tp2.Core
{
    public class IdentifierToken : Token
    {
        public string Identifier { get; }

        public IdentifierToken(string identifier) : base(TokenType.Identificateur)
        {
            Identifier = identifier;
        }

        public override bool Equals(object obj)
        {
            return obj is IdentifierToken identifierToken && identifierToken.Identifier.Equals(Identifier);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Type, Identifier);
        }
    }
}
