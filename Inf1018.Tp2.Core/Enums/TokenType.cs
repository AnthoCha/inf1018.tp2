﻿namespace Inf1018.Tp2.Core.Enums
{
    public enum TokenType
    {
        Procedure,
        FinProcedure,
        Declare,
        DeuxPoints,
        PointVirgule,
        Entier,
        Reel,
        Egale,
        Addition,
        Soustraction,
        Multiplication,
        Division,
        ParantheseOuvrante,
        ParantheseFermante,
        Identificateur,
        Nombre
    }
}
