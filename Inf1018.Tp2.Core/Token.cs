﻿using Inf1018.Tp2.Core.Enums;

namespace Inf1018.Tp2.Core
{
    public class Token
    {
        public TokenType Type { get; }

        public Token(TokenType type)
        {
            Type = type;
        }

        public override bool Equals(object obj)
        {
            return obj is Token token && token.Type.Equals(Type);
        }

        public override int GetHashCode()
        {
            return Type.GetHashCode();
        }
    }
}
