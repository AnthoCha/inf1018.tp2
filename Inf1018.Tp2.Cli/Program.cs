﻿using Inf1018.Tp2.Lex;
using Inf1018.Tp2.Resources;
using Inf1018.Tp2.Syntax;
using System;
using System.IO;

namespace Inf1018.Tp2.Cli
{
    class Program
    {
        static int Main(string[] args)
        {
            if (args.Length > 0)
            {
                try
                {
                    using var textReader = File.OpenText(args[0]);
                    try
                    {
                        var lexReader = new LexReader(textReader);
                        lexReader.Read();
                        var reader = new SyntaxReader(lexReader);
                        reader.Read();

                        Console.WriteLine(Texts.SyntaxSuccess);
                        return 0;
                    }
                    catch (ApplicationException ex)
                    {
                        Console.Error.WriteLine(ex);
                        Console.Error.WriteLine(string.Format(ErrorMessages.ErrorPosition, textReader.BaseStream.Position));
                    }
                }
                catch (IOException ex)
                {
                    Console.Error.WriteLine(ex);
                }
            }
            else
            {
                Console.Error.WriteLine(ErrorMessages.PathArgRequired);
            }

            return 1;
        }
    }
}
