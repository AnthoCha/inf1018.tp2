﻿using Inf1018.Tp2.Core.Enums;
using Inf1018.Tp2.Core.Interfaces;
using Inf1018.Tp2.Syntax.Exceptions;

namespace Inf1018.Tp2.Syntax
{
    public class ProcedureSyntaxReader : BaseSyntaxReader
    {
        private readonly SyntaxReaderContext _context;

        public ProcedureSyntaxReader(ILexReader reader, SyntaxReaderContext context) : base(reader)
        {
            _context = context;
        }

        public override void Read()
        {
            ReadToken(TokenType.Procedure);

            var identifier1 = ReadIdentifierToken();

            new DeclarationsSyntaxReader(Reader, _context).Read();

            new InstructionsAffectationSyntaxReader(Reader, _context).Read();

            ReadToken(TokenType.FinProcedure);

            var identifier2 = ReadIdentifierToken();

            if (identifier1.Identifier != identifier2.Identifier)
            {
                throw new ProcedureIdentifiersDoNotMatchException(identifier1.Identifier, identifier2.Identifier);
            }
        }
    }
}
