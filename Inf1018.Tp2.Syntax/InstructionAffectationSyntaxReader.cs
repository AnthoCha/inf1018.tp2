﻿using Inf1018.Tp2.Core.Enums;
using Inf1018.Tp2.Core.Interfaces;

namespace Inf1018.Tp2.Syntax
{
    public class InstructionAffectationSyntaxReader : BaseSyntaxReader
    {
        private readonly SyntaxReaderContext _context;

        public InstructionAffectationSyntaxReader(ILexReader reader, SyntaxReaderContext context) : base(reader)
        {
            _context = context;
        }

        public override void Read()
        {
            new VariableSyntaxReader(Reader, _context).Read();

            ReadToken(TokenType.Egale);

            new ExpressionArithmetiqueSyntaxReader(Reader, _context).Read();
        }
    }
}
