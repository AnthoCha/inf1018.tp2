﻿using Inf1018.Tp2.Core;
using Inf1018.Tp2.Core.Enums;
using Inf1018.Tp2.Core.Interfaces;
using Inf1018.Tp2.Syntax.Exceptions;
using System;
using System.Linq;

namespace Inf1018.Tp2.Syntax
{
    public abstract class BaseSyntaxReader : ISyntaxReader
    {
        protected ILexReader Reader { get; }

        public BaseSyntaxReader(ILexReader reader)
        {
            Reader = reader;
        }

        public abstract void Read();

        protected Token ReadToken(params TokenType[] expected)
        {
            var token = Reader.Current;

            if (token == null)
            {
                throw new MissingTokenException(expected);
            }

            if (!expected.Contains(token.Type))
            {
                throw new UnexpectedTokenException(token.Type, expected);
            }

            Reader.Read();

            return token;
        }

        protected IdentifierToken ReadIdentifierToken()
        {
            var token = Reader.Current;

            if (token == null)
            {
                throw new MissingTokenException(TokenType.Identificateur);
            }

            if (token is not IdentifierToken identifierToken)
            {
                throw new UnexpectedTokenException(token.Type, TokenType.Identificateur);
            }

            Reader.Read();

            return identifierToken;
        }
    }
}
