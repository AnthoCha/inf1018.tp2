﻿using Inf1018.Tp2.Core.Enums;
using Inf1018.Tp2.Core.Interfaces;

namespace Inf1018.Tp2.Syntax
{
    public class TypeSyntaxReader : BaseSyntaxReader
    {
        public TypeSyntaxReader(ILexReader reader) : base(reader)
        {
        }

        public override void Read()
        {
            ReadToken(TokenType.Entier, TokenType.Reel);
        }
    }
}
