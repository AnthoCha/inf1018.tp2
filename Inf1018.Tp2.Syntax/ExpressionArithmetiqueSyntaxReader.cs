﻿using Inf1018.Tp2.Core;
using Inf1018.Tp2.Core.Enums;
using Inf1018.Tp2.Core.Interfaces;

namespace Inf1018.Tp2.Syntax
{
    public class ExpressionArithmetiqueSyntaxReader : BaseSyntaxReader
    {
        private readonly SyntaxReaderContext _context;

        public ExpressionArithmetiqueSyntaxReader(ILexReader reader, SyntaxReaderContext context) : base(reader)
        {
            _context = context;
        }

        public override void Read()
        {
            new TermeSyntaxReader(Reader, _context).Read();

            Token token;
            if ((token = Reader.Current) != null && (token.Type == TokenType.Addition || token.Type == TokenType.Soustraction))
            {
                Reader.Read();
                new TermeSyntaxReader(Reader, _context).Read();
            }
        }
    }
}

