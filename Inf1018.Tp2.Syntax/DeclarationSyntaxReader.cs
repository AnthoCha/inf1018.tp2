﻿using Inf1018.Tp2.Core.Enums;
using Inf1018.Tp2.Core.Interfaces;

namespace Inf1018.Tp2.Syntax
{
    public class DeclarationSyntaxReader : BaseSyntaxReader
    {
        private readonly SyntaxReaderContext _context;

        public DeclarationSyntaxReader(ILexReader reader, SyntaxReaderContext context) : base(reader)
        {
            _context = context;
        }

        public override void Read()
        {
            ReadToken(TokenType.Declare);
            var variable = ReadIdentifierToken();
            _context.DeclareVariable(variable.Identifier);
            ReadToken(TokenType.DeuxPoints);
            new TypeSyntaxReader(Reader).Read();
            ReadToken(TokenType.PointVirgule);
        }
    }
}
