﻿using System.Collections.Generic;

namespace Inf1018.Tp2.Syntax
{
    public class SyntaxReaderContext
    {
        private readonly ISet<string> _variables;

        public SyntaxReaderContext()
        {
            _variables = new HashSet<string>();
        }

        public void DeclareVariable(string identifier)
        {
            _variables.Add(identifier);
        }

        public bool IsVariableDeclared(string identifier)
        {
            return _variables.Contains(identifier);
        }
    }
}
