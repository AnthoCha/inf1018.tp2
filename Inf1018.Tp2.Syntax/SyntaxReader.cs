﻿using Inf1018.Tp2.Core;
using Inf1018.Tp2.Core.Interfaces;
using Inf1018.Tp2.Syntax.Exceptions;

namespace Inf1018.Tp2.Syntax
{
    public class SyntaxReader : ISyntaxReader
    {
        private readonly ILexReader _reader;
        private readonly SyntaxReaderContext _context;

        public SyntaxReader(ILexReader reader)
        {
            _reader = reader;
            _context = new SyntaxReaderContext();
        }

        public void Read()
        {
            new ProcedureSyntaxReader(_reader, _context).Read();

            Token token;
            if ((token = _reader.Current) != null)
            {
                throw new UnexpectedTokenException(token.Type);
            }
        }
    }
}
