﻿using Inf1018.Tp2.Core.Enums;
using Inf1018.Tp2.Core.Interfaces;
using Inf1018.Tp2.Syntax.Exceptions;

namespace Inf1018.Tp2.Syntax
{
    public class FacteurSyntaxReader : BaseSyntaxReader
    {
        private readonly SyntaxReaderContext _context;

        public FacteurSyntaxReader(ILexReader reader, SyntaxReaderContext context) : base(reader)
        {
            _context = context;
        }

        public override void Read()
        {
            var token = Reader.Current;

            if (token == null)
            {
                throw new MissingTokenException(TokenType.Identificateur, TokenType.Nombre, TokenType.ParantheseOuvrante);
            }

            switch (token.Type)
            {
                case TokenType.Identificateur:
                    new VariableSyntaxReader(Reader, _context).Read();
                    break;
                case TokenType.Nombre:
                    Reader.Read();
                    break;
                case TokenType.ParantheseOuvrante:
                    Reader.Read();

                    new ExpressionArithmetiqueSyntaxReader(Reader, _context).Read();

                    ReadToken(TokenType.ParantheseFermante);
                    break;
                default:
                    throw new UnexpectedTokenException(token.Type, TokenType.Identificateur, TokenType.Nombre, TokenType.ParantheseOuvrante);
            }
        }
    }
}

