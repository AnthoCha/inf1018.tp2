﻿using Inf1018.Tp2.Core;
using Inf1018.Tp2.Core.Enums;
using Inf1018.Tp2.Core.Interfaces;

namespace Inf1018.Tp2.Syntax
{
    public class DeclarationsSyntaxReader : BaseSyntaxReader
    {
        private readonly SyntaxReaderContext _context;

        public DeclarationsSyntaxReader(ILexReader reader, SyntaxReaderContext context) : base(reader)
        {
            _context = context;
        }

        public override void Read()
        {
            new DeclarationSyntaxReader(Reader, _context).Read();

            Token token;
            if ((token = Reader.Current) != null && token.Type == TokenType.Declare)
            {
                new DeclarationsSyntaxReader(Reader, _context).Read();
            }
        }
    }
}
