﻿using Inf1018.Tp2.Core;
using Inf1018.Tp2.Core.Enums;
using Inf1018.Tp2.Core.Interfaces;

namespace Inf1018.Tp2.Syntax
{
    public class InstructionsAffectationSyntaxReader : BaseSyntaxReader
    {
        private readonly SyntaxReaderContext _context;

        public InstructionsAffectationSyntaxReader(ILexReader reader, SyntaxReaderContext context) : base(reader)
        {
            _context = context;
        }

        public override void Read()
        {
            new InstructionAffectationSyntaxReader(Reader, _context).Read();

            Token token;
            if ((token = Reader.Current) != null && token.Type == TokenType.PointVirgule)
            {
                Reader.Read();
                new InstructionAffectationSyntaxReader(Reader, _context).Read();
            }
        }
    }
}
