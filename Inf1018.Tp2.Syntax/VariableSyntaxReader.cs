﻿using Inf1018.Tp2.Core.Interfaces;
using Inf1018.Tp2.Syntax.Exceptions;

namespace Inf1018.Tp2.Syntax
{
    public class VariableSyntaxReader : BaseSyntaxReader
    {
        private readonly SyntaxReaderContext _context;

        public VariableSyntaxReader(ILexReader reader, SyntaxReaderContext context) : base(reader)
        {
            _context = context;
        }

        public override void Read()
        {
            var variable = ReadIdentifierToken();

            if (!_context.IsVariableDeclared(variable.Identifier))
            {
                throw new VariableNotDeclaredException(variable.Identifier);
            }
        }
    }
}

