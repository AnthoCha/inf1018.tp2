﻿using Inf1018.Tp2.Core.Enums;
using Inf1018.Tp2.Core.Exceptions;
using Inf1018.Tp2.Resources;
using System.Collections.Generic;

namespace Inf1018.Tp2.Syntax.Exceptions
{
    public class MissingTokenException : SyntaxException
    {
        public IEnumerable<TokenType> Expected { get; }

        public MissingTokenException(params TokenType[] expected)
        {
            Expected = expected;
        }

        public override string Message
        {
            get
            {
                return string.Join(' ', base.Message, ErrorMessages.MissingToken, string.Format(ErrorMessages.ExpectedToken, string.Join(", ", Expected)));
            }
        }
    }
}
