﻿using Inf1018.Tp2.Core.Exceptions;
using Inf1018.Tp2.Resources;

namespace Inf1018.Tp2.Syntax.Exceptions
{
    public class VariableNotDeclaredException : SyntaxException
    {
        public string Identifier { get; }


        public VariableNotDeclaredException(string identifier)
        {
            Identifier = identifier;
        }

        public override string Message
        {
            get
            {
                return string.Join(' ', base.Message, string.Format(ErrorMessages.VariableNotDeclared, Identifier));
            }
        }
    }
}
