﻿using Inf1018.Tp2.Core.Enums;
using Inf1018.Tp2.Core.Exceptions;
using Inf1018.Tp2.Resources;
using System.Collections.Generic;

namespace Inf1018.Tp2.Syntax.Exceptions
{
    public class UnexpectedTokenException : SyntaxException
    {
        public TokenType Actual { get; }

        public IEnumerable<TokenType> Expected { get; }


        public UnexpectedTokenException(TokenType actual, params TokenType[] expected)
        {
            Actual = actual;
            Expected = expected;
        }

        public override string Message
        {
            get
            {
                return string.Join(' ', base.Message, string.Format(ErrorMessages.UnexpectedToken, Actual), string.Format(ErrorMessages.ExpectedToken, string.Join(", ", Expected)));
            }
        }
    }
}
