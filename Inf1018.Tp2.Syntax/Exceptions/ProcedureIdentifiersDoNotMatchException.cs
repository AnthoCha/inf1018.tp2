﻿using Inf1018.Tp2.Core.Exceptions;
using Inf1018.Tp2.Resources;

namespace Inf1018.Tp2.Syntax.Exceptions
{
    public class ProcedureIdentifiersDoNotMatchException : SyntaxException
    {
        public string Identifier1 { get; }

        public string Identifier2 { get; }

        public ProcedureIdentifiersDoNotMatchException(string identifier1, string identifier2)
        {
            Identifier1 = identifier1;
            Identifier2 = identifier2;
        }

        public override string Message
        {
            get
            {
                return string.Join(' ', base.Message, string.Format(ErrorMessages.ProcedureIdentifiersDoNotMatch, Identifier1, Identifier2));
            }
        }
    }
}
