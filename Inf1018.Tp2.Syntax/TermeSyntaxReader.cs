﻿using Inf1018.Tp2.Core;
using Inf1018.Tp2.Core.Enums;
using Inf1018.Tp2.Core.Interfaces;

namespace Inf1018.Tp2.Syntax
{
    public class TermeSyntaxReader : BaseSyntaxReader
    {
        private readonly SyntaxReaderContext _context;

        public TermeSyntaxReader(ILexReader reader, SyntaxReaderContext context) : base(reader)
        {
            _context = context;
        }

        public override void Read()
        {
            new FacteurSyntaxReader(Reader, _context).Read();

            Token token;
            if ((token = Reader.Current) != null && (token.Type == TokenType.Multiplication || token.Type == TokenType.Division))
            {
                Reader.Read();
                new FacteurSyntaxReader(Reader, _context).Read();
            }
        }
    }
}

